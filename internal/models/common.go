package models

type Response struct {
	StatusCode string `valid:"Required" json:"status_code"`
	StatusDesc string `valid:"Required" json:"status_desc"`
}

type DefaultResponse struct {
	Data interface{} `json:"data"`
	Response
}

func CreateResponse(status string, message string, data interface{}) (response DefaultResponse) {
	response = DefaultResponse{
		Response: Response{
			StatusCode: status,
			StatusDesc: message,
		},
		Data: data,
	}

	return
}

