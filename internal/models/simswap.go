package models

// SimRequestNotify ...
type SimRequestNotify struct {
	Channel       string `json:"channel" validate:"required"`
	TransactionID string `json:"transaction_id" validate:"required"`
	Msisdn        string `json:"msisdn"  validate:"required"`
	Type          string `json:"type"  validate:"required"`
	Action        string `json:"action" validate:"required"`
	CallbackURL   string `json:"callback_url" validate:"-"`
}

// SimResponseNotify ...
type SimResponseNotify struct {
	StatusCode    string    `json:"status_code"`
	StatusDesc    string    `json:"status_desc"`
	TransactionID string    `json:"transaction_id"`
	Took          int64     `json:"took"`
	Data          []SimData `json:"data"`
}

// SimCallbackNotify ..
type SimCallbackNotify struct {
	Channel       string `json:"channel"`
	TransactionID string `json:"transaction_id"`
	RequestID     string `json:"request_id"`
	Msisdn        string `json:"msisdn"`
	Status        string `json:"status"`
}

// SimData ...
type SimData struct {
	PartnerID   string `json:"partner_id"`
	RequestID   string `json:"request_id"`
	IsFound     bool   `json:"is_found"`
	Status      bool   `json:"status"`
	UpdatedTime bool   `json:"updated_time"`
}

// SimRequestCheck ...
type SimRequestCheck struct {
	TransactionID string `validate:"required"`
	Msisdn        string `validate:"required"`
}

// SimResponseCheck ...
type SimResponseCheck struct {
	StatusCode    string    `json:"status_code"`
	StatusDesc    string    `json:"status_desc"`
	TransactionID string    `json:"transaction_id"`
	Took          int64     `json:"took"`
	Data          []SimData `json:"data"`
}
