package models

// ReadinessCheckResponse :nodoc:
type ReadinessCheckResponse struct {
	DBConnection *ReadinessStatus `json:"db_connection"`
}

// ReadinessStatus :nodoc:
type ReadinessStatus struct {
	Status  string `json:"status"`
	Message string `json:"message,omitempty"`
}
