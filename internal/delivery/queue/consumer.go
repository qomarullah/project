package queue

import (
	"fmt"
	"log"
	"os"
	"os/signal"

	cluster "github.com/bsm/sarama-cluster"
	"gitlab.com/qomarullah/project/internal/delivery"
	"gitlab.com/qomarullah/project/internal/domain/simswap"
	"gitlab.com/qomarullah/project/internal/shared/config"
	"gitlab.com/qomarullah/project/pkg/logger"
)

// ConsumerHandler ...
type consumerUsecase struct {
	Cfg            *config.DefaultConfig
	Logger         logger.Logger
	Consumer       *cluster.Consumer
	SimswapUsecase simswap.Usecase
}

// NewConsumer ...
func NewConsumer(container *delivery.Container) delivery.QueueConsumer {
	handler := &consumerUsecase{
		Cfg:            container.Cfg,
		Logger:         container.Logger,
		Consumer:       container.KafkaConsumer,
		SimswapUsecase: container.SimswapUsecase,
	}
	return handler
}

// Consume ...
func (a *consumerUsecase) Consume() {

	// trap SIGINT to trigger a shutdown.
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt)

	// consume errors
	go func() {
		for err := range a.Consumer.Errors() {
			fmt.Println("Error: %s\n", err.Error())
		}
	}()

	// consume notifications
	go func() {
		for ntf := range a.Consumer.Notifications() {
			log.Printf("Rebalanced: %+v\n", ntf)
		}
	}()

	// consume messages, watch signals
	for {
		select {
		case msg, ok := <-a.Consumer.Messages():
			if ok {
				fmt.Fprintf(os.Stdout, "%s/%d/%d\t%s\t%s\n", msg.Topic, msg.Partition, msg.Offset, msg.Key, msg.Value)
				a.Consumer.MarkOffset(msg, "") // mark message as processed
			}
		case <-signals:
			return
		}
	}

}
