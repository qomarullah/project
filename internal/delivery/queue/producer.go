package queue

import (
	"github.com/Shopify/sarama"
	"gitlab.com/qomarullah/project/internal/delivery"
	"gitlab.com/qomarullah/project/internal/shared/config"
	"gitlab.com/qomarullah/project/pkg/logger"
	"go.uber.org/zap"
)

// producerUsecase ...
type producerUsecase struct {
	Cfg      *config.DefaultConfig
	Logger   logger.Logger
	Producer sarama.SyncProducer
}

// NewProducer ...
func NewProducer(container *delivery.Container) delivery.QueueProducer {
	handler := &producerUsecase{
		Cfg:      container.Cfg,
		Logger:   container.Logger,
		Producer: container.KafkaProducer,
	}
	return handler
}

// SendMessage ...
func (a *producerUsecase) SendMessage(topic, msg string) error {

	kafkaMsg := &sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.StringEncoder(msg),
	}

	partition, offset, err := a.Producer.SendMessage(kafkaMsg)
	if err != nil {
		a.Logger.Debug("sendMessage", zap.Any("Send message error: %v", err))
		return err
	}

	a.Logger.Info("sendMessage", zap.Any("topic", topic), zap.Any("partititon", partition), zap.Any("offset", offset))
	return nil
}
