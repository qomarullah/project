package delivery

import "context"

// Server :nodoc:
type Server interface {
	Start(port string) error
	Stop(ctx context.Context) error
}
