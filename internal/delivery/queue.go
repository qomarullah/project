package delivery

// Queue :nodoc:
type QueueConsumer interface {
	Consume()
}
type QueueProducer interface {
	SendMessage(topic, msg string) error
}
