package delivery

import (
	"github.com/Shopify/sarama"
	cluster "github.com/bsm/sarama-cluster"
	_healthheckUsecase "gitlab.com/qomarullah/project/internal/domain/healthcheck/usecase"
	_simswapRepo "gitlab.com/qomarullah/project/internal/domain/simswap/repository"
	_simswapUsecase "gitlab.com/qomarullah/project/internal/domain/simswap/usecase"
	_database "gitlab.com/qomarullah/project/internal/shared/infrastructure/database"
	_queue "gitlab.com/qomarullah/project/internal/shared/infrastructure/queue"

	"gitlab.com/qomarullah/project/internal/domain/healthcheck"
	"gitlab.com/qomarullah/project/internal/domain/simswap"

	"gitlab.com/qomarullah/project/internal/shared/config"
	"gitlab.com/qomarullah/project/pkg/logger"
)

// Container :nodoc:
type Container struct {
	Cfg              *config.DefaultConfig
	Logger           logger.Logger
	Repo             simswap.Repository
	HealthcheckUcase healthcheck.Usecase
	SimswapUsecase   simswap.Usecase
	KafkaConsumer    *cluster.Consumer
	KafkaProducer    sarama.SyncProducer
}

// NewContainer :nodoc:
func NewContainer(cfg *config.DefaultConfig) *Container {

	// Setup Loger
	logger := logger.New(cfg.Logger)

	// Setup DB & Infra
	dbCon, dbErr := _database.New(cfg.DbConfig, _database.MySQL)
	if dbErr != nil {
		panic(dbErr)
	}
	//httpAdapter := integration.NewHTTPClient(&cfg,logger)

	// Setup queue
	var kafkaConsumer *cluster.Consumer
	var kafkaProducer sarama.SyncProducer
	var kafkaConsumerErr error
	var kafkaProducerErr error
	if !cfg.Queue.Skip {
		kafkaConsumer, kafkaConsumerErr = _queue.NewConsumerCluster(cfg.Queue)
		if kafkaConsumerErr != nil {
			panic(kafkaConsumerErr)
		}
		kafkaProducer, kafkaProducerErr = _queue.NewProducer(cfg.Queue)
		if kafkaProducerErr != nil {
			panic(kafkaProducerErr)
		}
	}
	// Setup Repository
	repo := _simswapRepo.New(dbCon, logger)

	// Setup Usecase
	healthcheckUcase := _healthheckUsecase.New()
	simswapUsecase := _simswapUsecase.New(cfg, logger)

	return &Container{
		Cfg:              cfg,
		Logger:           logger,
		Repo:             repo,
		HealthcheckUcase: healthcheckUcase,
		SimswapUsecase:   simswapUsecase,
		KafkaConsumer:    kafkaConsumer,
		KafkaProducer:    kafkaProducer,
	}

}
