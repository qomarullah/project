package rest

import (
	"bytes"
	"encoding/json"
	"net/http"

	"github.com/Shopify/sarama"
	"github.com/labstack/echo/v4"
	"github.com/spf13/cast"
	"gitlab.com/qomarullah/project/internal/models"
	"gitlab.com/qomarullah/project/internal/shared/errors"
	"gitlab.com/qomarullah/project/internal/shared/utils"
	"go.uber.org/zap"
)

func (h *server) Publish(c echo.Context) error {
	buff := new(bytes.Buffer)
	_, err := buff.ReadFrom(c.Request().Body)
	if err != nil {
		return errors.ErrParameterInvalid
	}

	//parse request to struct params & request
	msg := string(buff.Bytes())
	topic := c.QueryParam("topic")

	kafkaMsg := &sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.StringEncoder(msg),
	}
	partition, offset, err := h.kafkaProducer.SendMessage(kafkaMsg)

	if err != nil {
		h.logger.Error("sendMessage", zap.Any("Send message error: %v", err))
	}
	h.logger.Info("sendMessage", zap.Any("topic", topic), zap.Any("partititon", partition), zap.Any("offset", offset))

	return c.JSON(http.StatusOK, cast.ToString(partition)+cast.ToString(offset))
}

func (h *server) Notify(c echo.Context) error {
	buff := new(bytes.Buffer)
	_, err := buff.ReadFrom(c.Request().Body)
	if err != nil {
		return errors.ErrParameterInvalid
	}

	//parse request to struct params & request
	request := &models.SimRequestNotify{}
	if err = json.Unmarshal(buff.Bytes(), &request); err != nil {
		return errors.ErrParameterInvalid
	}

	if err = c.Validate(request); err != nil {
		return errors.ErrParameterInvalid
	}

	//out, err := h(utils.NewContextFromEcho(c), request)
	out := "test"
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, out)
}

func (h *server) Check(c echo.Context) error {

	//parse request to struct params & request
	request := &models.SimRequestCheck{
		TransactionID: c.QueryParam("transaction_id"),
		Msisdn:        c.Param("msisdn"),
	}

	if err := c.Validate(request); err != nil {
		return errors.ErrParameterInvalid
	}

	out, err := h.simswapUsecase.Check(utils.NewContextFromEcho(c), request)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, out)
}
