package rest

import (
	"net/http"

	"gitlab.com/qomarullah/project/internal/shared/utils"

	"github.com/labstack/echo/v4"
)

func (h *server) HealthCheck(c echo.Context) error {
	return c.String(http.StatusOK, "services up and running... ")
}

func (h *server) ReadinessCheck(c echo.Context) error {
	return c.JSON(http.StatusOK, h.hcu.ReadinessCheck(utils.NewContextFromEcho(c)))
}

func (h *server) Ping(c echo.Context) error {
	return c.String(http.StatusOK, "pong")
}
