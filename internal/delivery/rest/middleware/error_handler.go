package middleware

import (
	"net/http"

	"gitlab.com/qomarullah/project/internal/models"
	"gitlab.com/qomarullah/project/internal/shared/errors"

	"github.com/labstack/echo/v4"
)

// ErrorHandler :nodoc:
func ErrorHandler(err error, c echo.Context) {
	if c.Response().Committed || err == nil {
		return
	}
	// Set general error
	c.Set(ErrorMessageKey, err.Error())
	resp := models.DefaultResponse{
		Response: models.Response{
			StatusCode: errors.StatusGeneralError,
			StatusDesc: err.Error(),
		},
		//Data: struct{}{},
	}

	// Check if error exist
	statusCode, ok := errors.MapErrToStatusCode(err)
	if ok {
		resp.StatusCode = statusCode
	}

	_ = c.JSON(http.StatusOK, resp)
}
