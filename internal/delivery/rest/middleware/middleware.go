package middleware

import (
	"fmt"
	"strings"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/qomarullah/project/internal/shared/config"
	"gitlab.com/qomarullah/project/internal/shared/constants"
	"gitlab.com/qomarullah/project/internal/shared/errors"
	"gitlab.com/qomarullah/project/internal/shared/log"

	sharedUtils "gitlab.com/qomarullah/project/internal/shared/utils"
	"gitlab.com/qomarullah/project/pkg/logger"
	"gitlab.com/qomarullah/project/pkg/utils"
)

const (
	// ErrorMessageKey :nodoc:
	ErrorMessageKey = "error"
	beginTime       = "begin"
)

// Middleware :nodoc:
type Middleware struct {
	config *config.DefaultConfig
	logger logger.Logger
}

// Init :nodoc:
func Init(config *config.DefaultConfig, logger logger.Logger) *Middleware {
	return &Middleware{
		config: config,
		logger: logger,
	}
}

// TDRSkipper :nodoc:
func (m *Middleware) TDRSkipper(c echo.Context) bool {
	healthCheckURL := []string{"/", "/ready", "/ping"}
	isIncludingHealthcheck := sharedUtils.StringContains(healthCheckURL, c.Request().RequestURI)
	return isIncludingHealthcheck
}

// TDRLogHandler :nodoc:
func (m *Middleware) TDRLogHandler(c echo.Context, req, res []byte) {
	threadID, _ := c.Get(constants.ThreadIDKey).(string)
	errStr, _ := c.Get(ErrorMessageKey).(string)
	//contentType := c.Request().Header.Get(echo.HeaderContentType)

	tdr := logger.LogTdrModel{
		AppName:        m.config.Apps.Name,
		AppVersion:     "0.0",
		IP:             m.config.Apps.Address,
		LogTime:        time.Now().Format(time.RFC3339),
		Port:           m.config.Apps.HTTPPort,
		SrcIP:          c.RealIP(),
		Path:           c.Request().URL.String(),
		Header:         getRequestHeaders(c),
		Request:        strings.TrimSpace(string(req)),
		Response:       string(res),
		ResponseCode:   "",
		Error:          errStr,
		ThreadID:       threadID,
		AdditionalData: nil,
	}

	beginTime, ok := c.Get(beginTime).(time.Time)
	if ok {
		tdr.RespTime = time.Since(beginTime).Milliseconds()
	}

	m.logger.TDR(tdr)
}

// TDRLoggingMiddleware :nodoc:
func (m *Middleware) TDRLoggingMiddleware() middleware.BodyDumpConfig {
	return middleware.BodyDumpConfig{
		Skipper: m.TDRSkipper,
		Handler: m.TDRLogHandler,
	}
}

// RequestMiddleware :nodoc:
func (m *Middleware) RequestMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		// - Set session to context
		reqID := c.Request().Header.Get(echo.HeaderXRequestID)
		if len(reqID) == 0 {
			reqID = utils.GenerateThreadId()
		}
		c.Set(constants.ThreadIDKey, reqID)
		c.Set(beginTime, time.Now())

		return next(c)
	}
}

// ClientIDAuthMiddleware :nodoc:
func (m *Middleware) ClientIDAuthMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		clientID := c.Request().Header.Get("client-id")
		if clientID == "" {
			m.logger.Error("invalid client id", log.Source())
			return errors.ErrAuthInvalid
		}

		apiKey := c.Request().Header.Get("x-api-key")
		if apiKey == "" {
			m.logger.Error("invalid api key", log.Source())
			return errors.ErrAuthInvalid
		}

		/*if key, ok := m.config.ClientAPIKey[clientID]; ok && key == apiKey {
			return next(c)
		}
		*/

		m.logger.Error(fmt.Sprintf("authentication failed with client-id: %s, api-key: %s", clientID, apiKey), log.Source())
		return errors.ErrAuthInvalid
	}
}

func getRequestHeaders(c echo.Context) map[string]interface{} {
	headers := map[string]interface{}{}

	for key, val := range c.Request().Header {
		headers[key] = val[0]
	}

	return headers
}
