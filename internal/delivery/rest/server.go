package rest

import (
	"context"

	"gitlab.com/qomarullah/project/internal/delivery"

	httpMiddleware "gitlab.com/qomarullah/project/internal/delivery/rest/middleware"
	"gitlab.com/qomarullah/project/internal/shared/utils"
	"gopkg.in/go-playground/validator.v9"

	"github.com/Shopify/sarama"
	"github.com/labstack/echo/v4"
	"gitlab.com/qomarullah/project/pkg/logger"

	"github.com/labstack/echo/v4/middleware"
	_healthceckUsecase "gitlab.com/qomarullah/project/internal/domain/healthcheck"
	_simswapUsecase "gitlab.com/qomarullah/project/internal/domain/simswap"
	"gitlab.com/qomarullah/project/internal/shared/config"
)

type server struct {
	echoServer     *echo.Echo
	cfg            *config.DefaultConfig
	logger         logger.Logger
	hcu            _healthceckUsecase.Usecase
	simswapUsecase _simswapUsecase.Usecase
	kafkaProducer  sarama.SyncProducer
}

// New - creating new instance for apidb echoServer
func New(container *delivery.Container) delivery.Server {
	
	middL := httpMiddleware.Init(container.Cfg, container.Logger)
	echoServer := echo.New()

	echoServer.Use(middleware.BodyDumpWithConfig(middL.TDRLoggingMiddleware()))
	echoServer.Use(middL.RequestMiddleware)
	echoServer.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.POST, echo.OPTIONS},
		AllowHeaders: []string{
			echo.HeaderOrigin,
			echo.HeaderAuthorization,
			echo.HeaderAccessControlAllowOrigin,
			"token",
			"Pv",
			echo.HeaderContentType,
			echo.HeaderAccept,
			echo.HeaderContentLength,
			echo.HeaderAcceptEncoding,
			echo.HeaderXCSRFToken,
			echo.HeaderXRequestID,
		},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
	}))
	echoServer.Use(middleware.Recover())

	echoServer.HTTPErrorHandler = httpMiddleware.ErrorHandler
	echoServer.Validator = &utils.DataValidator{ValidatorData: validator.New()}
	echoServer.HideBanner = true

	h := server{
		echoServer:     echoServer,
		cfg:            container.Cfg,
		logger:         container.Logger,
		hcu:            container.HealthcheckUcase,
		simswapUsecase: container.SimswapUsecase,
		kafkaProducer:  container.KafkaProducer,
	}

	h.routes(middL)

	return &h
}

func (h *server) Start(port string) error {
	return h.echoServer.Start(port)
}

func (h *server) Stop(ctx context.Context) error {
	return h.echoServer.Shutdown(ctx)
}

func (h *server) routes(middL *httpMiddleware.Middleware) {

	h.echoServer.GET("/", h.HealthCheck)
	h.echoServer.GET("/ready", h.ReadinessCheck)
	h.echoServer.GET("/ping", h.Ping)

	h.echoServer.POST("/internal/v1/simswap/publish", h.Publish)
	h.echoServer.POST("/internal/v1/simswap/notify", h.Notify)
	h.echoServer.GET("/internal/v1/simswap/check/:msisdn", h.Check)

}
