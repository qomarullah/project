package usecase

import (
	"context"

	"gitlab.com/qomarullah/project/internal/domain/healthcheck"

	"gitlab.com/qomarullah/project/internal/models"
)

const (
	statusOK     = "OK"
	statusFailed = "Failed"
)

type healthCheckUsecase struct {
}

// New :nodoc:
func New() healthcheck.Usecase {

	return &healthCheckUsecase{}
}

func (h *healthCheckUsecase) ReadinessCheck(ctx context.Context) *models.ReadinessCheckResponse {
	return &models.ReadinessCheckResponse{}
}
