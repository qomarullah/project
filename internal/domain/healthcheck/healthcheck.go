package healthcheck

import (
	"context"

	"gitlab.com/qomarullah/project/internal/models"
)

// Usecase :nodoc:
type Usecase interface {
	ReadinessCheck(ctx context.Context) *models.ReadinessCheckResponse
}
