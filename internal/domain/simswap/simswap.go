package simswap

import (
	"context"

	"gitlab.com/qomarullah/project/internal/models"
)

// Usecase :nodoc:
type Usecase interface {
	Notify(ctx context.Context, req *models.SimRequestNotify) (resp *models.SimResponseNotify, err error)
	Check(ctx context.Context, req *models.SimRequestCheck) (resp *models.SimResponseCheck, err error)
}

// Repository :nodoc:
type Repository interface {
	GetByMSISDN(ctx context.Context, msisdn string) (resp *models.SimData, err error)
}
