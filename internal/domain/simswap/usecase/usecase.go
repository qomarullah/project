package usecase

import (
	"context"

	"gitlab.com/qomarullah/project/internal/domain/simswap"
	"gitlab.com/qomarullah/project/internal/models"
	"gitlab.com/qomarullah/project/internal/shared/config"
	"gitlab.com/qomarullah/project/internal/shared/errors"
	"gitlab.com/qomarullah/project/pkg/logger"
)

type simswapUsecase struct {
	config *config.DefaultConfig
	logger logger.Logger
}

func New(
	config *config.DefaultConfig,
	logger logger.Logger,
) simswap.Usecase {

	if config == nil {
		panic("config is nil")
	}

	if logger == nil {
		panic("logger is nil")
	}

	return &simswapUsecase{
		config: config,
		logger: logger,
	}
}

func (s *simswapUsecase) Notify(ctx context.Context, req *models.SimRequestNotify) (resp *models.SimResponseNotify, err error) {

	resp = &models.SimResponseNotify{}
	err = errors.ErrAuthInvalid
	return resp, err
}
func (s *simswapUsecase) Check(ctx context.Context, req *models.SimRequestCheck) (resp *models.SimResponseCheck, err error) {
	resp = &models.SimResponseCheck{}
	resp.StatusCode = errors.StatusSuccess
	resp.StatusDesc = errors.StatusSuccessMSg

	//err = errors.StatusSuccess
	return resp, err
}
