package repository

import (
	"context"
	"database/sql"

	"github.com/jmoiron/sqlx"
	"gitlab.com/qomarullah/project/internal/domain/simswap"
	"gitlab.com/qomarullah/project/internal/models"
	errShared "gitlab.com/qomarullah/project/internal/shared/errors"
	"gitlab.com/qomarullah/project/pkg/logger"
)

type repo struct {
	dbConn *sqlx.DB
	logger logger.Logger
}

func New(conn *sqlx.DB, logger logger.Logger) simswap.Repository {
	if logger == nil {
		panic("logger is nil")
	}
	return &repo{
		dbConn: conn,
		logger: logger,
	}
}

func (r *repo) CheckDBConn(ctx context.Context, dbConn *sql.DB) error {

	return dbConn.PingContext(ctx)

}

func (r *repo) GetByMSISDN(ctx context.Context, msisdn string) (*models.SimData, error) {

	res := &models.SimData{}
	err := errShared.ErrParameterInvalid
	/*if err != nil {
		r.logger.Error("connection not found", zap.Error(err), log.ThreadID(ctx), zap.String("msisdn",msisdn)), log.Source())
		return nil, errPkg.ErrDBConnectionNotFound
	}
	*/

	return res, err
}
