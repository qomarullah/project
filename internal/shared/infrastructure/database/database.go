package database

import (
	"fmt"
	"time"

	"github.com/matryer/try"

	"github.com/jmoiron/sqlx"

	"gitlab.com/qomarullah/project/internal/shared/config"

	_ "github.com/go-sql-driver/mysql" // mysql driver
	_ "github.com/godror/godror"       // oracle driver
	_ "github.com/lib/pq"              // postgres driver
)

//Type :nodoc:
type Type string

const (
	// PostgreSQL :nodoc:
	PostgreSQL = Type("pgsql")
	// MySQL :nodoc:
	MySQL = Type("mysql")
	// Oracle :nodoc:
	Oracle = Type("oracle")
)

// New :nodoc:
func New(cfg config.Database, dbType Type) (*sqlx.DB, error) {
	var (
		db  *sqlx.DB
		err error
	)

	switch dbType {
	case PostgreSQL:
		db, err = sqlx.Open("postgres",
			fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", cfg.Host, cfg.Port, cfg.Username, cfg.Password, cfg.Database))
	case MySQL:
		db, err = sqlx.Open("mysql",
			fmt.Sprintf("%s:%s@(%s:%v)/%s",
				cfg.Username,
				cfg.Password,
				cfg.Host,
				cfg.Port,
				cfg.Schema))
	case Oracle:
		db, err = sqlx.Open("godror",
			fmt.Sprintf("user=%s password=%s connectString=%s:%d/%s  libDir=%s",
				cfg.Username, cfg.Password, cfg.Host, cfg.Port, cfg.Database, cfg.LibDir))
	default:
		return nil, fmt.Errorf("unsupported database type %s", dbType)
	}
	if err != nil {
		fmt.Printf("failed to create db connection. name: %s\n", cfg.Database)
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		fmt.Printf("failed to ping db. name: %s\n", cfg.Database)
		return nil, err
	}

	db.SetMaxIdleConns(cfg.MaxIdleConn)
	db.SetMaxOpenConns(cfg.MaxOpenConn)
	if cfg.MaxLifeTime > 0 {
		db.SetConnMaxLifetime(time.Duration(cfg.MaxLifeTime) * time.Second)
	}

	fmt.Printf("db connection created. "+
		"name: %s, kind: %s, host: %s, port: %d, max_open_conn: %d, max_idle_conn: %d, conn_max_lifetime: %d\n",
		cfg.Database,
		cfg.Kind,
		cfg.Host,
		cfg.Port,
		cfg.MaxOpenConn,
		cfg.MaxIdleConn,
		cfg.MaxLifeTime,
	)
	return db, nil
}

// NewMulti :nodoc:
func NewMulti(dbs []config.Database) (map[string]*sqlx.DB, error) {
	m := make(map[string]*sqlx.DB, len(dbs))
	for _, db := range dbs {
		var conn *sqlx.DB
		err := try.Do(func(attempt int) (retry bool, err error) {
			switch db.Kind {
			case "postgres":
				conn, err = New(db, PostgreSQL)
			case "oracle":
				conn, err = New(db, Oracle)
			}
			return attempt <= 2, err
		})
		if err != nil {
			return nil, err
		}

		m[db.Database] = conn
	}
	return m, nil
}
