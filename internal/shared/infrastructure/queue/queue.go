package queue

import (
	"fmt"
	"time"

	"github.com/Shopify/sarama"
	cluster "github.com/bsm/sarama-cluster"
	"gitlab.com/qomarullah/project/internal/shared/config"
)

func NewConsumer(cfg config.Queue) (sarama.Consumer, error) {

	kafkaConfig := sarama.NewConfig()
	kafkaConfig.ClientID = cfg.ClientID
	kafkaConfig.Consumer.Return.Errors = true
	kafkaBrokers := cfg.Brokers
	kafkaConsumer, err := sarama.NewConsumer(kafkaBrokers, kafkaConfig)
	/*if err != nil {
		panic(err)
	}
	defer func() {
		if err := kafkaConsumer.Close(); err != nil {
			panic(err)
		}
	}()
	*/
	return kafkaConsumer, err

}

func NewConsumerCluster(cfg config.Queue) (*cluster.Consumer, error) {

	kafkaConfig := cluster.NewConfig()
	kafkaConfig.Consumer.Return.Errors = true
	kafkaConfig.Group.Return.Notifications = true
	kafkaConfig.Consumer.Return.Errors = true
	kafkaConfig.Consumer.Offsets.AutoCommit.Interval = 1 * time.Second
	kafkaConfig.Consumer.Offsets.CommitInterval = time.Second
	fmt.Println("broker", cfg.Brokers)
	fmt.Println("topic", cfg.Topics)

	kafkaConsumer, err := cluster.NewConsumer(cfg.Brokers, cfg.ClientID, cfg.Topics, kafkaConfig)
	/*if err != nil {
		panic(err)
	}
	defer func() {
		if err := kafkaConsumer.Close(); err != nil {
			panic(err)
		}
	}()
	*/

	return kafkaConsumer, err

}

func NewProducer(cfg config.Queue) (sarama.SyncProducer, error) {

	kafkaConfig := sarama.NewConfig()
	kafkaConfig.ClientID = cfg.ClientID
	kafkaConfig.Producer.Return.Errors = true
	kafkaConfig.Producer.Return.Successes = true
	kafkaBrokers := cfg.Brokers
	kafkaProducer, err := sarama.NewSyncProducer(kafkaBrokers, kafkaConfig)
	/*if err != nil {
		panic(err)
	}
	defer func() {
		if err := kafkaProducer.Close(); err != nil {
			panic(err)
		}
	}()
	*/
	return kafkaProducer, err

}
