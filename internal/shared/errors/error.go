package errors

import (
	"errors"
)

var (
	// Success :nodoc:
	//Success = errors.New("Success")
	// ErrAuthInvalid :nodoc:
	ErrAuthInvalid = errors.New("Invalid Authentication")
	// ErrParameterInvalid :nodoc:
	ErrParameterInvalid = errors.New("Invalid Parameter")
	// ErrDBError :nodoc:
	ErrDBError = errors.New("Internal Database Error")
	// ErrDatatNotFound :nodoc:
	ErrDatatNotFound = errors.New("Data Not Found")
	// ErrBackendTimeout :nodoc:
	ErrBackendTimeout = errors.New("Backend Timeout")
	// ErrBackendError :nodoc:
	ErrBackendError = errors.New("Backend Error")
	// ErrInternalError :nodoc:
	ErrInternalError = errors.New("Internal Error")

	mapErrToStatusCode = map[error]string{
		//Success:             StatusSuccess,
		ErrAuthInvalid:      StatusAuthInvalid,
		ErrParameterInvalid: StatusParameterInvalid,
		ErrDBError:          StatusDBError,
		ErrDatatNotFound:    StatusDatatNotFound,
		ErrBackendTimeout:   StatusBackendTimeout,
		ErrBackendError:     StatusBackendError,
		ErrInternalError:    StatusInternalError,
	}
)

// MapErrToStatusCode :nodoc:
func MapErrToStatusCode(err error) (string, bool) {
	s, ok := mapErrToStatusCode[err]
	return s, ok
}

// Map StatusCode
const (
	StatusSuccess          = "00"
	StatusSuccessMSg       = "Success"
	StatusAuthInvalid      = "ERR01"
	StatusParameterInvalid = "ERR02"
	StatusDBError          = "ERR03"
	StatusDatatNotFound    = "ERR04"
	StatusBackendTimeout   = "ERR05"
	StatusBackendError     = "ERR06"
	StatusInternalError    = "ERR07"
	StatusGeneralError     = "ERR99"
)
