package utils_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/qomarullah/project/internal/shared/constants"
	"gitlab.com/qomarullah/project/internal/shared/utils"
	"gopkg.in/go-playground/validator.v9"
)

func TestUtils_DataValidator(t *testing.T) {
	vali := utils.DataValidator{ValidatorData: validator.New()}

	type StructTest struct {
		Test  string `validate:"required"`
		Test2 string `validate:"required"`
	}

	testDeclaration := StructTest{
		Test:  "123",
		Test2: "123",
	}

	err := vali.Validate(testDeclaration)
	assert.NoError(t, err)
}
func TestUtils_NewContextFromEcho(t *testing.T) {
	server := echo.New()
	defer func() { _ = server.Close() }()

	req := httptest.NewRequest(http.MethodGet, "/ini-url", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	req.Header.Set(echo.HeaderXRequestID, "test")
	req.Header.Set(echo.HeaderXRealIP, "testip")

	rec := httptest.NewRecorder()
	c := server.NewContext(req, rec)

	threadIDKey := "testThreadID"
	c.Set(constants.ThreadIDKey, threadIDKey)
	ctx := utils.NewContextFromEcho(c)

	assert.Equal(t, ctx.Value(constants.ThreadIDKey), threadIDKey)
}

func TestUtils_StringContains(t *testing.T) {
	t.Run("true", func(t *testing.T) {
		str := []string{"satu", "dua", "tiga"}

		res := utils.StringContains(str, "dua")
		assert.Equal(t, true, res)
	})

	t.Run("false", func(t *testing.T) {
		str := []string{"satu", "dua", "tiga"}

		res := utils.StringContains(str, "lima")
		assert.Equal(t, false, res)
	})
}
