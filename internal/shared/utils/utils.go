package utils

import (
	"context"

	"github.com/labstack/echo/v4"
	"gitlab.com/qomarullah/project/internal/shared/constants"
	"gopkg.in/go-playground/validator.v9"
)

// DataValidator :nodoc:
type DataValidator struct {
	ValidatorData *validator.Validate
}

// Validate :nodoc:
func (cv *DataValidator) Validate(i interface{}) error {
	return cv.ValidatorData.Struct(i)
}

// NewContextFromEcho :nodoc:
func NewContextFromEcho(c echo.Context) context.Context {
	ctx := c.Request().Context()
	if threadID, ok := c.Get(constants.ThreadIDKey).(string); ok {
		ctx = context.WithValue(ctx, constants.ThreadIDKey, threadID)
	}
	return ctx
}

// StringContains :nodoc:
func StringContains(arr []string, val string) bool {
	for _, a := range arr {
		if a == val {
			return true
		}
	}
	return false
}
