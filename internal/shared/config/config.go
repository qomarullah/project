package config

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/viper"
	"gitlab.com/qomarullah/project/pkg/logger"
)

// DefaultConfig :nodoc:
type DefaultConfig struct {
	Apps     Apps           `json:"apps" yaml:"apps"`
	Logger   logger.Options `json:"logger" yaml:"logger"`
	DbConfig Database       `json:"dbConfig" yaml:"dbConfig"`
	Queue    Queue          `json:"queue"`
}

// Apps :nodoc:
type Apps struct {
	ID          string `json:"id" yaml:"id"`
	Name        string `json:"name" yaml:"name"`
	Address     string `json:"address" yaml:"address"`
	HTTPPort    int    `json:"httpPort" yaml:"httpPort"`
	HTTPTimeout int    `json:"httpTimeout" yaml:"httpTimeout"`
	TLS         bool   `json:"tls" yaml:"tls"`
}

// Database :nodoc:
type Database struct {
	Username    string `json:"username" yaml:"username"`
	Password    string `json:"password" yaml:"password"`
	Database    string `json:"database" yaml:"database"`
	Schema      string `json:"schema" yaml:"schema"`
	Host        string `json:"host" yaml:"host"`
	Port        int    `json:"port" yaml:"port"`
	MaxIdleConn int    `json:"maxIdleConn" yaml:"maxIdleConn"`
	MaxOpenConn int    `json:"maxOpenConn" yaml:"maxOpenConn"`
	MaxLifeTime int    `json:"maxLifeTime" yaml:"maxLifeTime"`
	LibDir      string `json:"libDir" yaml:"libDir"`
	Kind        string `json:"kind" yaml:"kind"`
}

type Queue struct {
	ClientID string   `json:"clientId"`
	Brokers  []string `json:"brokers"`
	Topics   []string `json:"topics"`
	Skip     bool     `json:"skip"`
}

// New :nodoc:
func New() *DefaultConfig {
	env := os.Getenv("ENV")
	if env == "" {
		env = "local"
	}

	if len(os.Args) > 1 {
		env = os.Args[1]
	}

	if strings.HasSuffix(os.Args[0], ".test") {
		env = "example"
	}

	viper.AddConfigPath("./configs")
	viper.SetConfigName("config." + env)

	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}

	defaultConfig := DefaultConfig{}
	err := viper.Unmarshal(&defaultConfig)
	if err != nil {
		panic(err)
	}
	return &defaultConfig
}

// AppAddress :nodoc:
func (c *DefaultConfig) AppAddress() string {
	return fmt.Sprintf(":%v", c.Apps.HTTPPort)
}
