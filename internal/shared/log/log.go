package log

import (
	"context"
	"fmt"
	"runtime"

	"gitlab.com/qomarullah/project/internal/shared/constants"

	"go.uber.org/zap"
)

// ThreadID :nodoc:
func ThreadID(ctx context.Context) zap.Field {
	if threadID, ok := ctx.Value(constants.ThreadIDKey).(string); ok {
		return zap.String(constants.ThreadIDKey, threadID)
	}

	return zap.Skip()
}

// Source :nodoc:
func Source() zap.Field {
	_, file, line, ok := runtime.Caller(1)
	if ok {
		return zap.String("source", fmt.Sprintf("[%s:%d]", file, line))
	}
	return zap.Skip()
}
