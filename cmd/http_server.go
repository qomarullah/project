package cmd

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"time"

	"gitlab.com/qomarullah/project/internal/delivery"

	"gitlab.com/qomarullah/project/internal/delivery/queue"
	"gitlab.com/qomarullah/project/internal/delivery/rest"

	"gitlab.com/qomarullah/project/internal/shared/config"
)

// StartService :nodoc:
func StartService() {
	cfg := config.New()
	container := delivery.NewContainer(cfg)

	// Setup Queue
	/*if !cfg.Queue.Skip {
		go func() {
			consumer := _queue.NewConsumer(container)
			fmt.Println("Queue consumer running...")
			doneCh := make(chan struct{})
			consumer.Consume(doneCh)
			<-doneCh
		}()
	}
	*/

	// Setup Server
	server := rest.New(container)
	go func() {
		//- start service
		if err := server.Start(cfg.AppAddress()); err != nil {
			fmt.Println("shutting down the server")
		}
	}()
	fmt.Println("HTTP server running...")

	consumer := queue.NewConsumer(container)
	fmt.Println("Queue consumer running...")
	consumer.Consume()

	//<-doneCh

	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, os.Interrupt)
	<-sigCh

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	if err := server.Stop(ctx); err != nil {
		fmt.Println(err.Error())
	}

}
