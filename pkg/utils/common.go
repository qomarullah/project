package utils

import (
	"fmt"
	"io"
	"math/rand"
	"strings"
	"time"

	rand2 "crypto/rand"

	"github.com/oklog/ulid"
)

const (
	// See http://golang.org/pkg/time/#Parse
	dateFormatyyyMMddHHmmSS = "20060102150405"
)

// RandomString get random string from given string length
// commonly used to generate requestId in json response
func RandomString(strlen int, id string) string {
	rand.Seed(time.Now().UTC().UnixNano())
	const chars = "abcdefghijklmnopqrstuvwxyz0123456789"
	result := make([]byte, strlen)
	for i := 0; i < strlen; i++ {
		result[i] = chars[rand.Intn(len(chars))] //nolint
	}
	return fmt.Sprintf("%s%s", id, string(result))
}

// GenerateTrxID :nodoc:
func GenerateTrxID(prefix string) string {
	t := time.Now()
	dt := t.Format(dateFormatyyyMMddHHmmSS)
	//guid := xid.New().String()
	guid := RandomString(8, "")
	txid := prefix + dt + guid
	return txid
}

// TimeToSecond :nodoc:
func TimeToSecond(t time.Duration) int64 {
	return t.Nanoseconds() / 1000000
}

// TimeStamp :nodoc:
func TimeStamp() string {
	t := time.Now()
	res := t.Format(dateFormatyyyMMddHHmmSS)
	return res
}

var table = [...]byte{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}

// GenerateThreadId ....
func GenerateThreadId() string {
	t := time.Now()
	entropy := rand.New(rand.NewSource(t.UnixNano()))
	uniqueID := ulid.MustNew(ulid.Timestamp(t), entropy)
	return uniqueID.String()
}

//TimestampRandomInt create a simple string timestamp + 2 digit random integer
func TimestampRandomInt() string {

	return fmt.Sprintf("%d%d", time.Now().Unix(), rand.New(rand.NewSource(time.Now().UnixNano())).Intn(99))
}

//GenerateRefNumber create random ref number usually being used as Bank reference number
func GenerateRefNumber() string {
	randomByTime := time.Now().Format("20060102150405.9999Z07")
	result := strings.Replace(randomByTime, ".", "", -1)
	result = strings.Replace(result, "+", "", -1)
	result = strings.Replace(result, "Z", "", -1)

	return result + "0" + encodeToString(5)
}

func encodeToString(max int) string {
	b := make([]byte, max)
	_, _ = io.ReadAtLeast(rand2.Reader, b, max)

	for i := 0; i < len(b); i++ {
		b[i] = table[int(b[i])%len(table)]
	}
	return string(b)
}
