package utils

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestRandomString(t *testing.T) {
	res := RandomString(5, "bulcomp")
	assert.Equal(t, 12, len(res))
}

func TestGenerateTrxID(t *testing.T) {
	res := GenerateTrxID("bulcomp")
	assert.Equal(t, 29, len(res))
}

func TestTimeToSecond(t *testing.T) {
	res := TimeToSecond(time.Since(time.Now()))
	assert.NotEqual(t, 0, res)
}

func TestTimeStamp(t *testing.T) {
	res := TimeStamp()
	assert.Equal(t, 14, len(res))
}
