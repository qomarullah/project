package utils

import (
	"encoding/base64"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTripleDesECBEncryptDecrypt(t *testing.T) {
	encKey := "MTIzNDU2NzgxMjM0NTY3ODEyMzQ1Njc4" // base64 "123456781234567812345678"
	key, err := base64.StdEncoding.DecodeString(encKey)
	assert.NoError(t, err)

	txt := "testpin"
	encrypt, err := TripleDesECBEncrypt([]byte(txt), key)
	assert.NoError(t, err)

	encrypted := base64.StdEncoding.EncodeToString(encrypt)

	dec, err := base64.StdEncoding.DecodeString(encrypted)
	assert.NoError(t, err)

	decrypted, err := TripleDesECBDecrypt(dec, key)
	assert.NoError(t, err)
	assert.Equal(t, string(decrypted), txt)
}
