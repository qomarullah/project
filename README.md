**Project**
Golang project template

### Structure Directory

├── cmd // app contains main execution file

├── configs // is a directory consist of the env value for the codebase

├── internal // is a directory consist of internal codebase

│   ├── domain // contains modular code based per domain/feature

│   │   ├── {domain-name} // is the domain or feature name

│   │   │   ├── delivery // is folder consist of the delivery process of the domain

│   │   │   │   ├── http // represent that domain feature will deliver with http endpoint

│   │   │   │   └── kafka // represent that domain feature will deliver with kafka (if needed)

│   │   │   ├── repository // is a domain's repository acting to store the data

│   │   │   └── usecase // is a domain's usecase acting for logical process of the feature

│   │   └── {domain-name}.go // is main file of domain feature represent of interface implementation from usecase and repository

│   ├── mock // is a directory consist of mock files result from mockgen

│   ├── models // is a directory consist of struct models used in codebase

│   └── shared // is a directory consist of shared/reusable package for the internal codebase

├── pkg // is a directory consist of shared/reusable package for the external codebase

└── resources // is a directory consist of static files (if needed)

<br>


### Mock Server for Load Test
Using [killgrave](https://github.com/friendsofgo/killgrave) to mock server for load test purposes. Run following command to simply run the mock server:
```
make run-mock-server
```
the configuration file is under resources/mock_server.

### Run with dev.json
Run with dev.json config `ENV=dev go run main.go`

### Build golang binary
`env GOOS=linux GOARCH=amd64 go build -o bulcomp`

## Configuration File (.json)
Read config file under resources/ folder, based on `ENV=` value <br/>
`ENV=prod` will read file `resources/config.prod.json`

### DB
for oracle connection need following setup
https://godror.github.io/godror/doc/installation.html

### Logging
- Configure `stdoutLog: true` options to write log to stdout or file
- Configure `syslog: ""` empty if stdout, sys log path if using file <br/>
- Configure `tdrlog: ""` empty if stdout, tdr log path if using file <br/>

### Database
DB: Mysql<br/>
- Configure maximum open connection (`maxOpenConn`) & maximum idle connection (`maxIdleConn`)