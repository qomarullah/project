SHELL:=/bin/bash

internal/mock/mock_repository_repository.go:
	mockgen -destination=internal/mock/mock_repository_repository.go -package=mock gitlab.com/qomarullah/project/internal/domain/repository Repository

internal/mock/mock_repository_auth.go:
	mockgen -destination=internal/mock/mock_repository_auth.go -package=mock gitlab.com/qomarullah/project/internal/domain/repository AuthRepository

internal/mock/mock_mfs_usecase.go:
	mockgen -destination=internal/mock/mock_mfs_usecase.go -package=mock gitlab.com/qomarullah/project/internal/domain/mfs MFSUsecase

internal/mock/mock_health_check_usecase.go:
	mockgen -destination=internal/mock/mock_health_check_usecase.go -package=mock gitlab.com/qomarullah/project/internal/domain/healthcheck HealthCheckUsecase

internal/shared/config/mock/mock_apidb.go:
	mockgen -destination=internal/shared/config/mock/mock_apidb.go -package=mock gitlab.com/qomarullah/project/internal/shared/config APIDBConfig

internal/mock/mock_mfs_integration.go:
	mockgen -destination=internal/shared/integration/mfs/mock/mfs.go -package=mock gitlab.com/qomarullah/project/internal/shared/integration/mfs HTTPClient

internal/mock/mock_forwarder_forwarder.go:
	mockgen -destination=internal/mock/mock_forwarder_forwarder.go -package=mock gitlab.com/qomarullah/project/internal/domain/forwarder Forwarder

internal/mock/mock_forwarder_request.go:
	mockgen -destination=internal/mock/mock_forwarder_request.go -package=mock gitlab.com/qomarullah/project/internal/domain/forwarder Request

mockgen: internal/mock/mock_repository_repository.go \
	internal/mock/mock_repository_auth.go \
	internal/mock/mock_mfs_usecase.go \
	internal/mock/mock_health_check_usecase.go \
	internal/shared/config/mock/mock_apidb.go \
	internal/mock/mock_mfs_integration.go \
	internal/mock/mock_forwarder_forwarder.go \
	internal/mock/mock_forwarder_request.go \

clean:
	rm -v internal/mock/mock*.go
	rm -v internal/shared/config/mock/mock*.go

run-mock-server:
	killgrave -config resources/mock_server/config.yml

test:
	ENV=test go test -coverprofile coverage.cov -cover ./... && go tool cover -func coverage.cov

lint:
	golangci-lint run --print-issued-lines=false --exclude-use-default=false --enable=golint --enable=goimports  --enable=unconvert --enable=unparam --enable=gosec --timeout=10m

packr:
	packr2

build: packr
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .