module gitlab.com/qomarullah/project

go 1.14

require (
	github.com/Shopify/sarama v1.27.2
	github.com/bsm/sarama-cluster v2.1.15+incompatible
	github.com/cheekybits/is v0.0.0-20150225183255-68e9c0620927 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/godror/godror v0.23.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/json-iterator/go v1.1.10
	github.com/labstack/echo/v4 v4.1.17
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.4 // indirect
	github.com/lib/pq v1.9.0
	github.com/matryer/try v0.0.0-20161228173917-9ac251b645a2
	github.com/oklog/ulid v1.3.1
	github.com/spf13/cast v1.3.0
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.7.0
	go.uber.org/zap v1.16.0
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
	honnef.co/go/tools v0.0.1-2019.2.3 // indirect
)
